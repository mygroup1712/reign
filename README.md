<h1>
<p align="center">
Reign Test App
</p>
<h1>

## Description

This app is composed of a server (hackerserver) an API (hackerapi) and a mongodb server.

## Installation and start

```bash
$ npm docker-compose up
```

Database will be filled automatically 30 seconds after the hackerserver start

## Coverage Test (general)

```bash
$ npm run test:cov

```

## Credentials for the API

```
user name: 'hackerapi'
password: 'hackerpwd'
```

## API url:

http://0.0.0.0:3000/api/docs

## First use:

1. Go to http://0.0.0.0:3000/api/docs/#/default/AppController_login

2. Click on "Try it out"

3. Insert this Json

```
{
  "username": "hackerapi",
  "password": "hackerpwd"
}
```

4. Pulse "Execute" button

5. Copy to the clipboard from the Response, the content of "access_token"

6. In the upper part of the UI, click the button "Authorize" and insert the content copied. Then push "Closed"

7. Now you can try the methods. If the response shows en error "401", repeat from the step 1.

## Environment variables (.env file)

```
API_URI="https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
SERVICE_HOST="http://hackerapi"
SERVICE_PORT=3000
DATABASE_URI="mongoserver/hackerdb"
```

## License

Nest is [MIT licensed](LICENSE).
