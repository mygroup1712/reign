import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Param,
  Query,
  Request,
  UseGuards,
  HttpStatus,
  HttpException,
} from "@nestjs/common";
import { AuthService } from "./auth/auth.service";
import { JwtAuthGuard } from "./auth/guards/jwt-auth.guard";
import { LocalAuthGuard } from "./auth/guards/local-auth.guard";
import { ArticlesService } from "./articles/articles.service";
import { ArticleDto } from "./articles/dto/article.dto";
import { QueryParams } from "./utils/queryParams";
import {
  ApiBody,
  ApiBearerAuth,
  ApiQuery,
  ApiOkResponse,
} from "@nestjs/swagger";
import { LoginDto } from "./auth/dto/logIn.dto";

@Controller()
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private readonly articlesService: ArticlesService
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post("auth/login")
  @ApiBody({
    type: LoginDto,
    examples: {
      a: {
        summary: "Required parameters",
        value: { username: "myuser", password: "mypassword" },
      },
    },
  })
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get("articles")
  @ApiBearerAuth("JWT")
  @ApiQuery({
    name: "limit",
    required: false,
    type: Number,
    description: "max number of articles",
  })
  @ApiQuery({
    name: "skip",
    required: false,
    type: Number,
    description: "number of articles to skip",
  })
  @ApiQuery({
    name: "author",
    required: false,
    type: String,
    description: "article's author",
  })
  @ApiQuery({
    name: "tag",
    required: false,
    type: String,
    description: "tag to filter",
  })
  @ApiQuery({
    name: "title",
    required: false,
    type: String,
    description: "article's title",
  })
  @ApiQuery({
    name: "month",
    required: false,
    type: String,
    description: "month name",
  })
  @ApiOkResponse({ type: ArticleDto, isArray: true })
  getArticles(
    @Query() { skip, limit, author, tag, title, month }: QueryParams
  ) {
    return this.articlesService.findAll(skip, limit, author, tag, title, month);
  }

  @Post("article")
  @ApiBearerAuth("JWT")
  @ApiBody({
    type: ArticleDto,
    examples: {
      a: {
        summary: "Small example",
        value: {
          created_at: "2022-06-06T21:50:36.000Z",
          title: "my title",
          author: "zamalek",
          points: null,
          story_text: null,
        },
      },
    },
  })
  @ApiOkResponse({ type: ArticleDto, isArray: false })
  async create(@Body() ArticleDto: ArticleDto) {
    return this.articlesService.create(ArticleDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete("article/:id")
  @ApiBearerAuth("JWT")
  async delete(@Param("id") id: string) {
    const result = await this.articlesService.delete(id);

    if (result["deletedCount"] == 0)
      throw new HttpException("Id not found", HttpStatus.NOT_FOUND);
  }
}
