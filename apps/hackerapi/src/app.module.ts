import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ArticlesService } from './articles/articles.service';
import { ArticlesProviders } from './providers/database/mongodb/articles/providers/articles.providers';
import { DatabaseModule } from './providers/database/database.modules';
import { ConfigModule } from '@nestjs/config'

@Module({
  
  imports: [ConfigModule.forRoot(), AuthModule, UsersModule, DatabaseModule],
  controllers: [AppController],
  providers: [ArticlesService, ...ArticlesProviders],
})
export class AppModule {}
