import { Injectable, Inject } from '@nestjs/common';
import { Model, Types  } from 'mongoose';
import { Article } from '../providers/database/mongodb/articles/interfaces/article.interface';
import { ArticleDto } from './dto/article.dto';
@Injectable()
export class ArticlesService {
    constructor(
        @Inject('ARTICLE_MODEL')
        private articletModel: Model<Article>,
      ) {}
    
        getMonthFromString(mon){
        const d = Date.parse(mon + "1, 2012");
        if(!isNaN(d)){
           return new Date(d).getMonth() + 1;
        }
        return -1;
        }

      async create(createCatDto: ArticleDto): Promise<Article> {
        const createdArt = this.articletModel.create(createCatDto);
        return createdArt;
      }

      async delete(id:string) {
        const ObjectId= Types.ObjectId;
        const id2 = new ObjectId(id)
        const deletedArt = await  this.articletModel.deleteOne({_id:id2});
        return deletedArt;
      }
      
      async findAll(documentsToSkip = 0, limitOfDocuments?: number, author?:string, tag?:string, title?:string, month?:string): Promise<Article[]> {
        console.log("limits:",documentsToSkip, limitOfDocuments, author, tag, title, month);
        const filter = {}
        
        if (author!=undefined) filter["author"]=author;
        if (tag!=undefined) filter["_tags"]=tag;
        if (title!=undefined) filter["title"]=title;
        if (month!=undefined) {
          const monthNumber = this.getMonthFromString(month);
          if (monthNumber!=-1) {
            const monthFilter = ('00' + this.getMonthFromString(month)).slice(-2);
            filter["created_at"]={$regex:"\\d{4}-"+monthFilter+"-.*"};  
          }
        }
       
        const query = this.articletModel.find(filter).skip(documentsToSkip);
        
        
        if (limitOfDocuments) {
          query.limit(limitOfDocuments);
        }
        
        return query.exec();
      }
}
