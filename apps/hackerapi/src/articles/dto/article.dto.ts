import  { ApiProperty } from '@nestjs/swagger';


export class story_url  {
    @ApiProperty()
    readonly value: string;
    @ApiProperty()
    readonly matchLevel: string;
    @ApiProperty()
    readonly matchedWords: [{
        type: string
    }]
}

export class story_title  {
    @ApiProperty()
    value: string;
    @ApiProperty()
    matchLevel: string;
    @ApiProperty()
    matchedWords:  [{
        type: string
    }]
}


export class comment_text {
    @ApiProperty()
    value: string;
    @ApiProperty()
    matchLevel: string;
    @ApiProperty()
    fullyHighlighted: boolean;
    @ApiProperty()
    matchedWords: [{
        type: string
    }]
}


export class authorDoc {
    @ApiProperty()
    value: string;
    @ApiProperty()
    matchLevel: string; 
    @ApiProperty()
    matchedWords:  [{
        type: string
    }]
}


export class highlightResult{
    @ApiProperty()
    author: authorDoc;
    @ApiProperty()
    comment_text: comment_text;
    @ApiProperty()
    story_title: story_title;
    @ApiProperty()
    story_url:story_url;
}

export class ArticleDto {
    @ApiProperty()
    'created_at':string;   
    @ApiProperty()
    'title':string;
    @ApiProperty()
    'url':string;        
    @ApiProperty()
    'author':string;
    @ApiProperty()
    'points':number;       
    @ApiProperty()
    'story_text':string;
    @ApiProperty()
    'comment_text':string; 
    @ApiProperty()
    'num_comments':number;
    @ApiProperty()
    'story_id':number; 
    @ApiProperty()
    'story_title':string;
    @ApiProperty()
    'story_url':string;  
    @ApiProperty()
    'parent_id':number;
    @ApiProperty()
    'created_at_i':number; 
    @ApiProperty()
    '_tags':  [{
        type: string
    }]
    @ApiProperty()
    'objectID':string;     
    @ApiProperty()
    '_highlightResult':highlightResult;
}
