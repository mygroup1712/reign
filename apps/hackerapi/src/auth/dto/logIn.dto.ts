import { IsString } from 'class-validator';
import  { ApiProperty } from '@nestjs/swagger';
export class LoginDto {

  @IsString()
  @ApiProperty()
  public username: string;


  @IsString()
  @ApiProperty()
  public password: string;
}

