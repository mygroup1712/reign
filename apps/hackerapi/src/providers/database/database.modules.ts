import { Module } from '@nestjs/common';
import { mongodbProviders } from './mongodb/mongodb.providers';

@Module({
  providers: [...mongodbProviders],
  exports: [...mongodbProviders],
})
export class DatabaseModule {}