
import * as mongoose from 'mongoose'
export interface story_url extends mongoose.Types.Subdocument {
    value: string,
    matchLevel: string,
    matchedWords: [{
        type: string
    }]
}

export interface story_title extends mongoose.Types.Subdocument {
    value: string,
    matchLevel: string,
    matchedWords:  [{
        type: string
    }]
}


export interface comment_text extends mongoose.Types.Subdocument {
    value: string,
    matchLevel: string,
    fullyHighlighted: boolean,
    matchedWords: [{
        type: string
    }]
}


export interface authorDoc extends mongoose.Types.Subdocument {
     value: string, 
     matchLevel: string, 
     matchedWords:  [{
        type: string
    }]
}


export interface highlightResult extends mongoose.Types.Subdocument {
    author: authorDoc,
    comment_text: comment_text,
    story_title: story_title,
    story_url:story_url
}

export interface Article extends Document {
    'created_at':string,   
    'title':string,
    'url':string,          
    'author':string,
    'points':number,       
    'story_text':string,
    'comment_text':string, 
    'num_comments':number,
    'story_id':number,     
    'story_title':string,
    'story_url':string,    
    'parent_id':number,
    'created_at_i':number, 
    '_tags':  [{
        type: string
    }]
    'objectID':string,     
    '_highlightResult':highlightResult
}