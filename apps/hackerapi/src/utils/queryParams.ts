import { IsNumber, Min, Max, IsOptional } from "class-validator";
import { Type } from "class-transformer";

export class QueryParams {
  @IsOptional()
  author: string;

  @IsOptional()
  tag: string;

  @IsOptional()
  title: string;

  @IsOptional()
  month: string;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(0)
  skip?: number;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(1)
  @Max(5)
  limit?: number;
}
