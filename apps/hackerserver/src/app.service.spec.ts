import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';
import { AppService } from './app.service';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), HttpModule, ScheduleModule.forRoot()],
      providers: [AppService],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  describe('GET from algolia API', () => {
    it('should return hits', () => {
      const dataJson = { data: require('../test/mocks/api.json') };
      const response: AxiosResponse<any> = {
        data: require('../test/mocks/api.json'),
        headers: {},
        config: { url: 'http://localhost:3000/mockUrl' },
        status: 200,
        statusText: 'OK',
      };
      jest.spyOn(appService, 'findAll').mockImplementation(() => of(response));

      let data = {};
      appService.findAll().subscribe({
        next: (val) => {
          data = val;
        },
        complete: () => {
          expect(data['data'].hits).toEqual(dataJson.data.hits);
        },
      });
    });

    it('should return nothing', () => {
      const response: AxiosResponse<any> = {
        data: undefined,
        headers: {},
        config: { url: 'http://localhost:3000/mockUrl' },
        status: 200,
        statusText: 'OK',
      };
      jest.spyOn(appService, 'findAll').mockImplementation(() => of(response));

      let data = {};
      appService.findAll().subscribe({
        next: (val) => {
          data = val;
        },
        complete: () => {
          expect(data['data']).toEqual(undefined);
          expect(data['config']['url']).toEqual(
            'http://localhost:3000/mockUrl',
          );
        },
      });
    });
  });

  describe('POST from hacker API', () => {
    it('should return 200', () => {
      const dataJson = { data: require('../test/mocks/api.json') };
      const response: AxiosResponse<any> = {
        data: dataJson,
        headers: {},
        config: { url: 'http://localhost:3000/mockUrl' },
        status: 200,
        statusText: 'OK',
      };
      jest
        .spyOn(appService, 'postToAPI')
        .mockImplementation(() => of(response));

      let data = {};
      appService.postToAPI({}).subscribe({
        next: (val) => {
          data = val;
        },
        complete: () => {
          expect(data['status']).toEqual(200);
        },
      });
    });
  });
});
