import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { HttpService } from "@nestjs/axios";
import { Observable } from "rxjs";
import { AxiosResponse } from "axios";
import { Cron, Timeout } from "@nestjs/schedule";

@Injectable()
export class AppService {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService // 👈 inject ConfigService
  ) {}

  findAll(): Observable<AxiosResponse<any>> {
    const uri = this.configService.get<string>("API_URI");
    return this.httpService.get(uri);
  }

  postToAPI(data): Observable<AxiosResponse<any>> {
    const uri =
      this.configService.get<string>("SERVICE_HOST") +
      ":" +
      this.configService.get<string>("SERVICE_PORT") +
      "/article";
    return this.httpService.post(uri, data);
  }

  obtainAndAndSave() {
    console.log("Processing data");
    const respAPI = this.findAll();

    respAPI.subscribe((value) => {
      if (value.data != undefined) {
        const data = value.data.hits;
        for (const item of data) {
          this.postToAPI(item).subscribe((value) => {
            if (value.status != 201) {
              console.log("Error posting record in API: ", value);
            }
          });
        }
      } else {
        console.log("error reading value");
      }
    });
  }

  @Cron("0 0 * * * *")
  handleCron() {
    try {
      this.obtainAndAndSave();
    } catch (error) {
      console.log(error);
    }
  }
}
